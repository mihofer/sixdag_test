# SixDAG_Test

This is the small repository to test a sixdesk prototype using [DAGMAN].

## Prerequisites

Before running this example, the user needs to have a python 3.7 installation with the packages `pylhc-submitter`, `tfs-pandas`, and 
`optics-functions` installed.
This is easily accomplished by following command

```bash
pip install pylhc-submitter tfs-pandas optics-functions
```

Then, to obtain a local copy, issue

```bash
git clone https://gitlab.cern.ch/mihofer/sixdag_test.git
```

Check that all shell scripts in the repository are executable and if not, run `chmod u+x *.sh`.

Last, replace the keyword `PYTHON` in `pre_mad6t.sh`, `pre_betabeat.sh`, and `betabeat.ini` with a link to your installation which includes the above mentioned packages.
Similarly, the path `AFS/U/USER` in `pre_mad6t.sh`, `pre_betabeat.sh`, `mad6t.ini`, `betabeat.ini` as well as in `sixdag.dag` should be replaced with a path to your afs space where the `sixdag_test` directory is located.

## To Run the example

To run the example, simply run `condor_submit_dag sixdag.dag`.
The jobs will then create two directories called `mad6t` and `betabeat`, where the input and output files of each job will be saved.
Alternatively, for more granular control, the `pre_` scripts can be run and a `.sub`-file will be created in the main directory, which can then be submitted with `condor_submit file.sub`.

## Behind the scenes

The logic of the automated DAGMan process is defined in the `.dag` file.
Here, the `mad6t` jobs needs to run first as it will produce the input for the following jobs `betabeat` and `sixtrack`.
Once the `mad6t` job finishes, these jobs will then automatically start.

Before each job, a `pre_` script is executed which will set up the folder structure for this job and create the HTCondor Submission file.
In this example, the `pre_` script will call the `pylhc-submitter`, which will setup the jobs according to what is defined in the `.ini` files.
The files in the `masks` folder are used as input for these studies.
Important: the `pylhc-submitter` will not submit the jobs (`dryrun=True`), but only create the `.sub` file which is then handed to DAGMan.
In case of the `sixtrack` job, the `pre_` script will issue an errorcode which is caught in the `.dag` file.
The `sixtrack` job is then skipped.

A `post_` script can be called after the jobs have finished to check if all files are present, however this is not yet implemented.

[dagman]: https://htcondor.readthedocs.io/en/latest/users-manual/dagman-workflows.html
