import tfs
from optics_functions.rdt import calculate_rdts
from optics_functions.utils import prepare_twiss_dataframe, split_complex_columns

# read MAD-X twiss output
df_twiss = tfs.read("%(TWISSFILE)s", index="NAME")


rdts = ["F3000"]

# check correct signs (i.e if beam==4), merge twiss and errors, 
# add empty K(S)L columns if needed
df_twiss = prepare_twiss_dataframe(df_twiss=df_twiss, df_errors=None, max_order=3)

# do the actual rdt calculation
df_rdts = calculate_rdts(df_twiss, rdts=rdts,
                         loop_phases=True,  # loop over phase-advance calculation, slower but saves memory
                         complex_columns=True,  # complex output
                         )


tfs.write("Outputdata/rdts.tfs",
          split_complex_columns(df_rdts, columns=rdts),
          save_index="NAME"
          )